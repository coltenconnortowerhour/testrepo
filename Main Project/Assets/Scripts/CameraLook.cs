﻿using UnityEngine;
using System.Collections;

public class CameraLook : MonoBehaviour {
	public float sensitivity = 15F;
	
	public float minimumY = -90F;
	public float maximumY = 90F;
	
	float rotationX = 0F;
	float rotationY = 0F;
	
	Quaternion originalRotation;
	Quaternion originalBodyRotation;

	public bool catchCursor = true;
	private bool isCursorCatched;

	public GameObject body;
	
	void Update ()
	{
		if (Input.GetMouseButtonDown (0)) {
			Debug.Log ("bruh");
			isCursorCatched = true;
			Cursor.lockState = (CursorLockMode.Locked);
			Cursor.visible = false;
		}
		if (Input.GetKeyDown (KeyCode.Escape)) {
			isCursorCatched = false;
			Cursor.lockState = (CursorLockMode.None);
			Cursor.visible = true;
		}

		if (catchCursor) {
			if (!isCursorCatched) {
				return;
			}
		}



		rotationX += Input.GetAxis("Mouse X")*sensitivity;
		rotationY += Input.GetAxis("Mouse Y")*sensitivity;
			
		rotationY = ClampAngle (rotationY, minimumY, maximumY);
			
		Quaternion xQuaternion = Quaternion.AngleAxis(rotationX, Vector3.up);
		Quaternion yQuaternion = Quaternion.AngleAxis(rotationY, Vector3.left);
			
		transform.localRotation = originalRotation*yQuaternion;

		// Spaghetti Code
		body.transform.localRotation = originalBodyRotation*xQuaternion;
	}
	
	void Start ()
	{
		originalRotation = transform.localRotation;
		originalBodyRotation = body.transform.localRotation;
	}
	
	public static float ClampAngle (float angle, float min, float max)
	{
		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;
		return Mathf.Clamp (angle, min, max);
	}
}
