﻿using UnityEngine;
using System.Collections;

public class BodyMovement : MonoBehaviour {
	public float forwardSpeed = 10;
	public float leftSpeed = 8;
	public float jumpHeight = 10;
	public float gravity = 20;

	private Vector3 moveDirection = Vector3.zero;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		CharacterController controller = GetComponent<CharacterController>();

		Vector3 forwarddir = transform.TransformDirection(Vector3.forward);
		Vector3 leftdir = transform.TransformDirection(Vector3.left);
		float forward = forwardSpeed * Input.GetAxis("Vertical");
		float left = leftSpeed * -Input.GetAxis("Horizontal");
		controller.SimpleMove((forward * forwarddir)+(left*leftdir));

		if (Input.GetButton ("Jump")) 
		if (controller.isGrounded){ 
			moveDirection.y= jumpHeight;
		}
		
		
		moveDirection.y -= gravity * Time.deltaTime;
		
		
		controller.Move(moveDirection * Time.deltaTime);
	}
}
